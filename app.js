'use strict';

const express = require('express');

// Constants
const PORT = 5000;

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World from Node!');
});

app.listen(PORT);
console.log(`Running on port: ${PORT}`);